---
layout: markdown_page
title: "Analytics"
---


For more details about our data and analytics strategy, please see the [Data Team project](https://gitlab.com/gitlab-data/analytics) and the [Data Team handbook page](/business-ops/data-team/)
